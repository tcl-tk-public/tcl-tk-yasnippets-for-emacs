# -*- mode: snippet -*-
# name: for {…} {…} {…}
# key: for
# --
for {set ${1:i} ${2:0}} {$1 < ${3:N}}; {incr $1} {
    $0
}
